<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 № A-8</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 № A-8</h1></div>
    <div></div>
</header>

<main>
    <form action="result.php" method="post">
        <label><h2>Введите текст для анализа</h2></label>
        <textarea title="text_for_analysis" name="data" id="text_for_analysis" cols="30" rows="10" placeholder="Введите текст... "></textarea>
        <input type="submit" value="Анализировать">

    </form>
</main>

<footer>

</footer>

</body>
</html>