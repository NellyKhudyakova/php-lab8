<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 № A-8</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 № A-8</h1></div>
    <div></div>
</header>

<main>

    <?php

    function test_num($text)
    {
        $number = array('0' => true, '1' => true, '2' => true, '3' => true, '4' => true,
            '5' => true, '6' => true, '7' => true, '8' => true, '9' => true);
        $number_amount = 0;

        for ($i = 0; $i < strlen($text); $i++) {
            if (array_key_exists($text[$i], $number)) {
                $number_amount++;
            }
        }
        return $number_amount;
    }

    function test_punctuation($text)
    {
        $punctuation_mark = array(',' => true, '.' => true, ':' => true, ';' => true, '-' => true,
            '!' => true, '?' => true, '(' => true, ')' => true, '"' => true);
        $punctuation_mark_amount = 0;

        for ($i = 0; $i < strlen($text); $i++) {
            if (array_key_exists($text[$i], $punctuation_mark)) {
                $punctuation_mark_amount++;
            }
        }
        return $punctuation_mark_amount;
    }

    function test_symbs($text)
    {
        $symbs = array();
        $l_text = mb_strtolower(str_replace("\r\n", "", $text), "cp1251");

        for ($i = 0; $i < strlen($l_text); $i++) {
            if (isset($symbs[$l_text[$i]])) {
                $symbs[$l_text[$i]]++;
            } else {
                $symbs[$l_text[$i]] = 1;
            }
        }
        return $symbs;
    }

    function test_words($text)
    {
        $word = '';
        $words = array();

        for ($i = 0; $i < strlen($text); $i++) {

            if ((($text[$i] == ' ') || ($i == strlen($text) - 1)) && (!is_numeric($text[$i]))) {
                if ($word) {
                    if (isset($words[$word]))
                        $words[$word]++;
                    else
                        $words[$word] = 1;
                }
                $word = '';
            } else {
                $word .= $text[$i];
            }
        }
//        $words_amount = count($words);
        return $words;
    }

    function is_letter($text)
    {
        setlocale(LC_ALL, 'ru', 'en');
        $letter_counter = 0;
        for ($i = 0; $i < strlen($text); $i++) {
            if (ctype_alpha($text[$i])) {
                $letter_counter++;
            }
        }
        return $letter_counter;
    }
    function is_letter_upper($text)
    {
        setlocale(LC_ALL, 'ru', 'en');
        $upper_counter = 0;
        for ($i = 0; $i < strlen($text); $i++) {
            if (ctype_upper($text[$i])) {
                $upper_counter++;
            }
        }
        return $upper_counter;
    }
    function is_letter_lower($text)
    {
        setlocale(LC_ALL, 'ru', 'en');
        $lower_counter = 0;
        for ($i = 0; $i < strlen($text); $i++) {
            if (ctype_lower($text[$i])) {
                $lower_counter++;
            }
        }
        return $lower_counter;
    }


    //    -----------------------------------------------------------------------

    function test_it($text)
    {

        $text = iconv("utf-8", "cp1251", $text);

        echo '<div class="analysis_results">';
            echo 'Количество символов: ' . strlen($text) . '<br>';
            echo 'Количество букв: ' . is_letter($text) . '<br>';
            echo 'Количество заглавных букв: ' . is_letter_upper($text) . '<br>';
            echo 'Количество строчных букв: ' . is_letter_lower($text) . '<br>';
            echo 'Количество знаков препинания: ' . test_punctuation($text) . '<br>';
            echo 'Количество цифр: ' . test_num($text) . '<br>';
            echo 'Количество слов: ' . count(test_words($text)) . '<br>';
            echo 'Список слов: <br>';
            echo '<pre>';
            $words_result = test_words($text);
            $array_keys = array_keys($words_result);

            for ($i = 0; $i < count($array_keys); $i++) {
                echo iconv("cp1251", "utf-8", $array_keys[$i]) . '=' . $words_result[$array_keys[$i]] . '<br>';
            }
            echo '</pre>';

            echo 'Количество вхождений каждого символа текста: <br>';
            echo '<pre>';
            $symbol_result = test_symbs($text);
            $array_keys = array_keys($symbol_result);

            for ($i = 0; $i < count($array_keys); $i++) {
                echo iconv("cp1251", "utf-8", $array_keys[$i]) . '=' . $symbol_result[$array_keys[$i]] . '<br>';
            }
            echo '</pre>';

        echo '</div>';
    }

//    -----------------------------------------------------------------------

    if (isset($_POST['data']) && $_POST['data']) {

        $text = $_POST['data'];

        echo '<pre class="src_text">' . $text . '</pre>'; // исходный текст
        test_it($_POST['data']);                          // результат анализа текста

    } else {
        echo '<div class="src_error">Нет текста для анализа</div>';
    }
    echo '<a class="back-button" href="/php-lab8/index.php">Другой анализ</a>';

    ?>

</main>

<footer>

</footer>

</body>
</html>
